using System;
using Cysharp.Threading.Tasks;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZuBle;

public class ZuBleTestUIController : MonoBehaviour
{
    public TextMeshProUGUI readResult;
    public TextMeshProUGUI status;
    public TextMeshProUGUI devicesList;
    public TextMeshProUGUI rssiResultLabel;
    public TextMeshProUGUI batteryResultLabel;
    
    public TMP_InputField deviceIdField;
    public TMP_InputField serviceIdField;
    public TMP_InputField characteristicIdField;
    public TMP_InputField valueField;
    
    public Button startScanButton;
    public Button stopScanButton;
    
    public Button connectButton;
    public Button disconnectButton;
    
    public Button readRssiButton;
    public Button readBatteryButton;
    public Button writeButton;
    public Button readCharacteristicButton;

    private string _deviceAddress;
    
    private void Start()
    {
        BleManager.Initialize();
        Application.logMessageReceived += OnMessage;
    }

    private void OnDestroy()
    {
        Application.logMessageReceived -= OnMessage;
    }

    private void OnEnable()
    {
        startScanButton.onClick.AddListener(StartScan);
        stopScanButton.onClick.AddListener(StopScan);
        
        connectButton.onClick.AddListener(ConnectToDevice);
        disconnectButton.onClick.AddListener(Disconnect);

        readRssiButton.onClick.AddListener(RequestRssi);
        readBatteryButton.onClick.AddListener(RequestBattery);
        
        writeButton.onClick.AddListener(WriteToDevice);
        readCharacteristicButton.onClick.AddListener(ReadCharacteristic);
        
        BleManager.OnDeviceFound += RequestRefreshDevices;
        BleManager.OnRssiResult += RequestUpdateRssiText;
        BleManager.OnConnected += ShowConnectedStatusText;
        BleManager.OnDisconnected += ShowDisconnectedStatus;
        BleManager.OnServicesDiscovered += ShowServicesDiscovered;
        
        BleManager.OnCharacteristicRead += OnCharacteristicRead;
    }

    private void OnDisable()
    {
        startScanButton.onClick.RemoveListener(StartScan);
        stopScanButton.onClick.RemoveListener(StopScan);
        
        connectButton.onClick.RemoveListener(ConnectToDevice);
        disconnectButton.onClick.RemoveListener(Disconnect);
        
        readRssiButton.onClick.RemoveListener(RequestRssi);
        readBatteryButton.onClick.RemoveListener(RequestBattery);
        
        writeButton.onClick.RemoveListener(WriteToDevice);
        readCharacteristicButton.onClick.RemoveListener(ReadCharacteristic);
        
        BleManager.OnDeviceFound -= RequestRefreshDevices;
        BleManager.OnRssiResult -= RequestUpdateRssiText;
        BleManager.OnConnected -= ShowConnectedStatusText;
        BleManager.OnDisconnected -= ShowDisconnectedStatus;
        BleManager.OnServicesDiscovered -= ShowServicesDiscovered;
        
        BleManager.OnCharacteristicRead -= OnCharacteristicRead;
    }

    private void OnMessage(string message, string stacktrace, LogType type)
    {
        if (type is LogType.Error or LogType.Exception)
        {
            status.text += $"\nERROR: {message}";
        }
    }
    
    private void StartScan()
    {
        status.text = "Scan started.";
        BleManager.StartScanning();
    }
    
    private void StopScan()
    {
        status.text = "Scan stopped!";
        BleManager.StopScanning();
    }
    
    private void RequestBattery()
    {
        ReadBatteryTask().Forget();
    }
    
    private async UniTaskVoid ReadBatteryTask()
    {
        var result = await BleManager.ReadBatteryCharacteristic(_deviceAddress);
        batteryResultLabel.text = $"Battery: {result}%";
    }
    
    private void RequestRssi()
    {
        BleManager.RequestRssi(_deviceAddress);
    }

    private void Disconnect()
    {
        BleManager.DisconnectDevice(_deviceAddress);
        var success = BleManager.GetConnectedDevices().Contains(_deviceAddress);
        status.text = $"Disconnected: {(success ? "Success": "Failed")}";
    }
    
    private void ShowServicesDiscovered(string address)
    {
        status.text += "| services discovered.";
    }

    private void ShowDisconnectedStatus(string address)
    {
        status.text = $"Disconnected: {address}";
    }

    private void ShowConnectedStatusText(string address)
    {
        _deviceAddress = address;
        status.text = $"Connected: {address}";
    }

    private void RequestUpdateRssiText(int rssi)
    {
        rssiResultLabel.text = $"Rssi: {rssi} dBm";
    }

    private void RequestRefreshDevices(BleDevice _)
    {
        devicesList.text = string.Join(",\n", BleManager.GetScannedDevices());
    }

    private void ConnectToDevice()
    {
        var devices = BleManager.GetScannedDevices();
        var deviceIndex = int.Parse(deviceIdField.text);

        if (deviceIndex < 0 || deviceIndex >= devices.Count)
        {
            Debug.LogError($"Invalid device index! {deviceIndex}");
            status.text = $"Invalid device index! {deviceIndex}";
        }

        var device = devices[deviceIndex];
        BleManager.RequestConnectToDevice(device.Address);
    }

    private void WriteToDevice()
    {
        Debug.Log("Write characteristic requested.");
        BleManager.WriteCharacteristic(_deviceAddress, serviceIdField.text, characteristicIdField.text, valueField.text);
    }
    
    private void ReadCharacteristic()
    {
        Debug.Log("Read characteristic requested.");
        BleManager.RequestReadCharacteristic(_deviceAddress, serviceIdField.text, characteristicIdField.text);
    }

    private void OnCharacteristicRead(string device, string characteristicId, byte[] value)
    {
        readResult.text = $"Read characteristic: {BitConverter.ToString(value).Replace("-", " ")}";
    }
}
