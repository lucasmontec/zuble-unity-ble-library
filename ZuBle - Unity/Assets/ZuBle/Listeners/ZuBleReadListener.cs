﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;

namespace ZuBle.Listeners
{
    // ReSharper disable InconsistentNaming
    public class ZuBleReadListener : AndroidJavaProxy
    {
        public event ReadCallback OnRead;
        public event CharacteristicOperationFailedCallback OnReadFailed;

        private readonly AsyncTaskQueue<byte[]> readPendingTasks = new();
        
        public ZuBleReadListener() : base(Java.WithPackage("BLEReadCallback")) {}
        
        public void AddReadTask(UniTaskCompletionSource<byte[]> task){
            readPendingTasks.Add(task);
        }
        
        [UsedImplicitly]
        public void onRead(string deviceAddress, string characteristicUuid, byte[] data)
        {
#if ZUBLE_DEBUG
            Debug.Log($"Read from {characteristicUuid} of {deviceAddress}: {BitConverter.ToString(data)}");
#endif
            OnRead?.Invoke(deviceAddress, characteristicUuid, data);
            bool completedAll = readPendingTasks.Complete(data);
            if (!completedAll)
            {
                Debug.LogError("One or more listener tasks failed to be completed!");
            }
        }

        [UsedImplicitly]
        public void onReadFailed(string deviceAddress, string characteristicUuid, int status, CancellationToken cancellationToken = default)
        {
            Debug.LogWarning($"Read failed from {characteristicUuid} of {deviceAddress}: status {status}");
            OnReadFailed?.Invoke(deviceAddress, characteristicUuid, status);
            bool cancelledAll = readPendingTasks.CancelAll(cancellationToken);
            if (!cancelledAll)
            {
                Debug.LogError("One or more listener tasks failed to be cancelled!");
            }
        }
    }
}