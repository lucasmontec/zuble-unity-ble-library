﻿using System.Collections.Generic;
using System.Threading;
using Cysharp.Threading.Tasks;

namespace ZuBle.Listeners
{
    public class AsyncTaskQueue<T>
    {
        private readonly Queue<UniTaskCompletionSource<T>> _pendingTasks = new();

        public void Add(UniTaskCompletionSource<T> taskCompletionSource)
        {
            _pendingTasks.Enqueue(taskCompletionSource);
        }

        public bool CancelAll(CancellationToken cancellationToken=default)
        {
            bool allSuccess = true;
            while (_pendingTasks.Count > 0)
            {
                var task = _pendingTasks.Dequeue();
                if (!task.TrySetCanceled(cancellationToken))
                {
                    allSuccess = false;
                }
            }

            return allSuccess;
        }

        public bool Complete(T value)
        {
            bool allSuccess = true;
            while (_pendingTasks.Count > 0)
            {
                var task = _pendingTasks.Dequeue();
                if (!task.TrySetResult(value))
                {
                    allSuccess = false;
                }
            }

            return allSuccess;
        }
    }

    public class AsyncTaskQueue
    {
        private readonly Queue<UniTaskCompletionSource> _pendingTasks = new();
        
        public void Add(UniTaskCompletionSource taskCompletionSource)
        {
            _pendingTasks.Enqueue(taskCompletionSource);
        }

        public bool CancelAll(CancellationToken cancellationToken=default)
        {
            bool allSuccess = true;
            while (_pendingTasks.Count > 0)
            {
                var task = _pendingTasks.Dequeue();
                if (!task.TrySetCanceled(cancellationToken))
                {
                    allSuccess = false;
                }
            }

            return allSuccess;
        }
        
        public bool Complete()
        {
            bool allSuccess = true;
            while (_pendingTasks.Count > 0)
            {
                var task = _pendingTasks.Dequeue();
                if (!task.TrySetResult())
                {
                    allSuccess = false;
                }
            }

            return allSuccess;
        }
    }
}