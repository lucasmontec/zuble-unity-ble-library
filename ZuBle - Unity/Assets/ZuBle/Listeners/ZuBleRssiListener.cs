﻿using System;
using System.Threading;
using Cysharp.Threading.Tasks;
using JetBrains.Annotations;
using UnityEngine;
using UnityEngine.Scripting;

namespace ZuBle.Listeners
{
    public class ZuBleRssiListener : AndroidJavaProxy
    {
        public event Action<int> OnRssiResult;

        private readonly AsyncTaskQueue<int> _rssiTaskQueue = new();
        
        public ZuBleRssiListener() : base(Java.WithPackage("BLERssiUpdateListener")) {}

        [UsedImplicitly, Preserve]
        // ReSharper disable once InconsistentNaming
        public bool onRssiUpdated(int rssi)
        {
#if ZUBLE_DEBUG
            Debug.Log($"Rssi response: {rssi}");
#endif
            OnRssiResult?.Invoke(rssi);
            return _rssiTaskQueue.Complete(rssi);
        }

        public void AddPendingTask(UniTaskCompletionSource<int> taskCompletionSource)
        {
            _rssiTaskQueue.Add(taskCompletionSource);
        }

        public bool CancelAllPendingTasks(CancellationToken cancellationToken=default)
        {
            return _rssiTaskQueue.CancelAll(cancellationToken);
        }
    }
}