using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using Cysharp.Threading.Tasks;
using UnityEngine;
using ZuBle.Listeners;
using ZuBle.UnityThreading;

//See: https://punchthrough.com/android-ble-guide/

namespace ZuBle
{
    public static class BleManager
    {
        private const string DisconnectDeviceMethod = "disconnectDevice";
        private const string ConnectToDeviceMethod = "connectToDevice";
        
        private const string GetScannedDevicesMethod = "getScannedDevices";
        private const string GetConnectedDevicesMethod = "getConnectedDevices";
        
        private const string GetLastErrorMethod = "getLastError";
        
        private const string ReadCharacteristicMethod = "readCharacteristic";
        private const string WriteCharacteristicMethod = "writeCharacteristic";
        private const string ReadRssiMethod = "readRemoteRssi";
        
        private const string SetDeviceFoundCallbackMethod = "setDeviceFoundCallback";
        private const string SetConnectionCallbackMethod = "setConnectionCallback";
        private const string SetReadCallbackMethod = "setReadCallback";
        private const string SetWriteCallbackMethod = "setWriteCallback";
        private const string SetRssiListenerMethod = "setRssiListener";

        private const string CheckServicesDiscoveredMethod = "areServicesDiscovered";
        private const string RequestMtuMethod = "requestMtu";
       
        private const string BatteryServiceUuid = "180F";  // 16-bit UUID for Battery Service
        private const string BatteryCharacteristicUuid = "2A19";  // 16-bit UUID for Battery Level

        private static AndroidJavaObject bleManager;

        private static readonly ZuBleDeviceFoundListener deviceFoundListener = new();
        private static readonly ZuBleConnectionListener connectionListener = new();
        
        private static readonly ZuBleReadListener readListener = new();
        private static readonly ZuBleWriteListener writeListener = new();
        private static readonly ZuBleRssiListener readRssiListener = new();

        private static bool initialized;
        
        public static event Action<BleDevice> OnDeviceFound;
        public static event Action<int> OnRssiResult; 
        
        public static event Action<string> OnConnected;
        public static event Action<string> OnDisconnected;
        public static event Action<string> OnServicesDiscovered;
        public static event Action<string, int> OnServiceDiscoveryFailed;
        
        public static event ReadCallback OnCharacteristicRead;
        public static event CharacteristicOperationFailedCallback OnCharacteristicReadFailed;

        public static event WriteCallback OnCharacteristicWrite;
        public static event CharacteristicOperationFailedCallback OnCharacteristicWriteFailed;
        
        public static void Initialize()
        {
            using var unityPlayer = new AndroidJavaClass(Java.UnityPlayerClass);
            var currentActivity = GetCurrentActivity(unityPlayer);
            bleManager = new AndroidJavaObject(Java.BleManagerClass, currentActivity);
            
            bleManager.Call(SetDeviceFoundCallbackMethod, deviceFoundListener);
            bleManager.Call(SetConnectionCallbackMethod, connectionListener);
            bleManager.Call(SetReadCallbackMethod, readListener);
            bleManager.Call(SetWriteCallbackMethod, writeListener);
            bleManager.Call(SetRssiListenerMethod, readRssiListener);

            deviceFoundListener.OnDeviceFound += 
                device => UnityThreadExecutor.AddActionOnUpdate(HandleDeviceFound, device);
            readRssiListener.OnRssiResult += rssi => UnityThreadExecutor.AddActionOnUpdate(HandleRssiResult, rssi);

            connectionListener.OnConnected += 
                address => UnityThreadExecutor.AddActionOnUpdate(HandleConnected, address);
            connectionListener.OnDisconnected += 
                address => UnityThreadExecutor.AddActionOnUpdate(HandleDisconnected, address);
            connectionListener.OnServicesDiscovered += 
                address => UnityThreadExecutor.AddActionOnUpdate(HandleServicesDiscovered, address);
            connectionListener.OnServiceDiscoveryFailed += 
                (address, code) => 
                    UnityThreadExecutor.AddActionOnUpdate(HandleServiceDiscoveryFailed, address, code);

            readListener.OnRead += (address, charId, value) => 
                UnityThreadExecutor.AddActionOnUpdate(() => OnCharacteristicRead?.Invoke(address, charId, value));
            readListener.OnReadFailed += (address, charId, status) => 
                UnityThreadExecutor.AddActionOnUpdate(() => OnCharacteristicReadFailed?.Invoke(address, charId, status));

            writeListener.OnCharacteristicWritten += (address, charId) => 
                UnityThreadExecutor.AddActionOnUpdate(() => OnCharacteristicWrite?.Invoke(address, charId));
            writeListener.OnCharacteristicWriteFailed += (address, charId, status) => 
                UnityThreadExecutor.AddActionOnUpdate(() => OnCharacteristicWriteFailed?.Invoke(address, charId, status));

            
            UnityThreadExecutor.Initialize();
            initialized = true;
        }
        
        private static void HandleConnected(string message)
        {
            OnConnected?.Invoke(message);
        }

        private static void HandleDisconnected(string message)
        {
            OnDisconnected?.Invoke(message);
        }

        private static void HandleServicesDiscovered(string address)
        {
            OnServicesDiscovered?.Invoke(address);
        }

        private static void HandleServiceDiscoveryFailed(string address, int status)
        {
            OnServiceDiscoveryFailed?.Invoke(address, status);
        }
        
        private static void HandleDeviceFound(BleDevice device)
        {
            OnDeviceFound?.Invoke(device);
        }

        private static void HandleRssiResult(int rssi)
        {
            OnRssiResult?.Invoke(rssi);
        }

        [MethodImpl(MethodImplOptions.AggressiveInlining)]
        private static AndroidJavaObject GetCurrentActivity(AndroidJavaObject unityPlayer)
        {
            return unityPlayer.GetStatic<AndroidJavaObject>("currentActivity");
        }
        
        public static void RequestRssi(string deviceAddress)
        {
            EnsureInitialized();
            bleManager?.Call(ReadRssiMethod, deviceAddress);
        }

        public static async UniTask<int> GetRssi(string deviceAddress)
        {
            var taskSource = new UniTaskCompletionSource<int>();
            readRssiListener.AddPendingTask(taskSource);
            RequestRssi(deviceAddress);
            
            await UniTask.SwitchToMainThread();
            return await taskSource.Task;
        }

        private static void EnsureInitialized()
        {
            if (initialized) return;
            Initialize();
        }

        public static bool RequestReadCharacteristic(string deviceAddress, string serviceUuid, string characteristicUuid)
        {
            EnsureInitialized();
            
            return bleManager.Call<bool>(ReadCharacteristicMethod, 
                deviceAddress, 
                serviceUuid,
                characteristicUuid);
        }

        /// <summary>
        /// Returns battery from 0 to 100,
        /// IF THE SERVICE PROVIDES IT!
        /// </summary>
        /// <param name="deviceAddress"></param>
        /// <returns></returns>
        public static async UniTask<int> ReadBatteryCharacteristic(string deviceAddress)
        {
            try
            {
                var result = 
                    await ReadCharacteristic(deviceAddress, BatteryServiceUuid, BatteryCharacteristicUuid);
                
                await UniTask.SwitchToMainThread();
                return result[0];
            }
            catch (Exception e)
            {
                Debug.LogError($"Failed to read battery from device {deviceAddress}: {e.Message} {e.StackTrace} {e.InnerException} {e.Source}");
                return -1;
            }
        }
        
        public static async UniTask<byte[]> ReadCharacteristic(string deviceAddress, string serviceUuid, string characteristicUuid)
        {
            EnsureInitialized();
            
            if (bleManager == null)
            {
                throw new Exception($"Null manager!");
            }
            
            //JAVA: public boolean readCharacteristic(String address, String serviceUuidString, String characteristicUuidString)
            var success = bleManager.Call<bool>(ReadCharacteristicMethod, 
                deviceAddress, 
                serviceUuid,
                characteristicUuid);

            if (!success)
            {
                throw new Exception("Read failed!");
            }
            
            var taskSource = new UniTaskCompletionSource<byte[]>();
            readListener.AddReadTask(taskSource);
            
            await UniTask.SwitchToMainThread();
            return await taskSource.Task;
        }
        
        public static void WriteCharacteristic(string deviceAddress, string serviceUuid, string characteristicUuid, byte[] data)
        {
            EnsureInitialized();
            string base64Data = Convert.ToBase64String(data);
            bleManager.Call(WriteCharacteristicMethod, deviceAddress, serviceUuid, characteristicUuid, base64Data, false);
        }
        
        public static async UniTask AsyncWriteCharacteristic(string deviceAddress, string serviceUuid, string characteristicUuid, byte[] data)
        {
            EnsureInitialized();
            var writeTask = new UniTaskCompletionSource();
            writeListener.AddWriteTask(writeTask);
            string base64Data = Convert.ToBase64String(data);
            bleManager.Call(WriteCharacteristicMethod, deviceAddress, serviceUuid, characteristicUuid, base64Data, false);
            await UniTask.SwitchToMainThread();
            await writeTask.Task;
        }

        public static void WriteCharacteristic(string deviceAddress, string serviceUuid, string characteristicUuid, string data, bool sendBase64EncodedString=false)
        {
            EnsureInitialized();
            string base64Data = Convert.ToBase64String(System.Text.Encoding.UTF8.GetBytes(data));
            bleManager.Call
                (WriteCharacteristicMethod, deviceAddress, serviceUuid, characteristicUuid, base64Data, sendBase64EncodedString);
        }
        
        public static void RequestConnectToDevice(string deviceAddress)
        {
            EnsureInitialized();
            bleManager?.Call(ConnectToDeviceMethod, deviceAddress);
        }
        
        public static async UniTask<string> ConnectToDevice(string deviceAddress)
        {
            EnsureInitialized();
            
            var taskSource = new UniTaskCompletionSource<string>();
            connectionListener.AddPendingConnectionTask(taskSource);
            
            bleManager?.Call(ConnectToDeviceMethod, deviceAddress);
            
            await UniTask.SwitchToMainThread();
            return await taskSource.Task;
        }
        
        public static void DisconnectDevice(string deviceAddress)
        {
            EnsureInitialized();
            
            bleManager?.Call(DisconnectDeviceMethod, deviceAddress);
            readRssiListener.CancelAllPendingTasks();
        }

        public static List<BleDevice> GetScannedDevices()
        {
            EnsureInitialized();
            
            List<BleDevice> result = new();
            string scannedDevices = bleManager.Call<string>(GetScannedDevicesMethod);
            string[] devices = scannedDevices.Split(',');
#if ZUBLE_DEBUG
            Debug.Log(scannedDevices);
            Debug.Log($"Scanned {devices.Length} devices.");
#endif
            foreach (string device in devices)
            {
                string[] info = device.Split('|');
                if (info.Length != 2) continue;
                string deviceName = info[0];
                string deviceAddress = info[1];
                result.Add(new BleDevice(deviceAddress, deviceName));
#if ZUBLE_DEBUG
                Debug.Log("Device Name: " + deviceName + ", Device Address: " + deviceAddress);
#endif
            }

            return result;
        }

        public static void RequestMtuChange(string deviceAddress, int mtu)
        {
            EnsureInitialized();
            bleManager.Call(RequestMtuMethod, deviceAddress, mtu);
        }
        
        public static async UniTask<bool> ChangeMtu(string deviceAddress, int mtu)
        {
            EnsureInitialized();
            
            var requestTask = new UniTaskCompletionSource<bool>();
            connectionListener.AddMtuChangeTask(requestTask);
            bleManager.Call(RequestMtuMethod, deviceAddress, mtu);
            
            await UniTask.SwitchToMainThread();
            return await requestTask.Task;
        }
        
        public static bool AreServicesDiscovered(string deviceAddress)
        {
            EnsureInitialized();
            return bleManager.Call<bool>(CheckServicesDiscoveredMethod, deviceAddress);
        }
        
        public static List<string> GetConnectedDevices()
        {
            EnsureInitialized();
            string connectedDeviceAddresses = bleManager.Call<string>(GetConnectedDevicesMethod);
            string[] devices = connectedDeviceAddresses.Split(',');
            return devices.ToList();
        }
        
        public static void StartScanning()
        {
            EnsureInitialized();
            bleManager.Call("startScanning");
        }
        
        public static void StopScanning()
        {
            EnsureInitialized();
            bleManager.Call("stopScanning");
        }
    }
}
