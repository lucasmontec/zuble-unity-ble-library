﻿namespace ZuBle
{
    public static class Java
    {
        public const string UnityPlayerClass = "com.unity3d.player.UnityPlayer";
        
        private const string Package = "com.carvalab.zuble";
        
        public const string BleManagerClass = Package + ".BLEManager";

        public static string WithPackage(string javaPath) => Package + "." + javaPath;
    }
}