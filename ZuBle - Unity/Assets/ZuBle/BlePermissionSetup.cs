﻿using System.Linq;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Events;

namespace ZuBle
{
    public class BlePermissionSetup : MonoBehaviour
    {
        private const string BluetoothScanPermission = "android.permission.BLUETOOTH_SCAN";
        private const string BluetoothPermission = "android.permission.BLUETOOTH";
        private const string BluetoothAdminPermission = "android.permission.BLUETOOTH_ADMIN";
        private const string BluetoothConnectPermission = "android.permission.BLUETOOTH_CONNECT";
        
        private const string LocationFinePermission = "android.permission.ACCESS_FINE_LOCATION";
        private const string LocationCoarsePermission = "android.permission.ACCESS_COARSE_LOCATION";

        private PermissionCallbacks _callbacks;

        public UnityEvent onPermissionsGranted;
        
        private void Start()
        {
            _callbacks = new PermissionCallbacks();
            _callbacks.PermissionDenied += PermissionCallbacks_PermissionDenied;
            _callbacks.PermissionGranted += PermissionCallbacks_PermissionGranted;
            _callbacks.PermissionDeniedAndDontAskAgain += PermissionCallbacks_PermissionDeniedAndDontAskAgain;
            
            RequestPermissionIfNotPermitted(
                BluetoothScanPermission, 
                BluetoothPermission,
                BluetoothAdminPermission,
                BluetoothConnectPermission,
                LocationFinePermission,
                LocationCoarsePermission
                );
        }

        private void OnDestroy()
        {
            _callbacks.PermissionDenied -= PermissionCallbacks_PermissionDenied;
            _callbacks.PermissionGranted -= PermissionCallbacks_PermissionGranted;
            _callbacks.PermissionDeniedAndDontAskAgain -= PermissionCallbacks_PermissionDeniedAndDontAskAgain;
            _callbacks = null;
        }

        private void RequestPermissionIfNotPermitted(params string[] permissions)
        {
            if (permissions.All(Permission.HasUserAuthorizedPermission))
            {
                onPermissionsGranted?.Invoke();
                return;
            }
            Permission.RequestUserPermissions(permissions, _callbacks);
        }

        private static void PermissionCallbacks_PermissionDeniedAndDontAskAgain(string permissionName)
        {
            Debug.Log($"{permissionName} PermissionDenied and DontAskAgain");
        }

        private void PermissionCallbacks_PermissionGranted(string permissionName)
        {
            Debug.Log($"{permissionName} Permission Granted");
            onPermissionsGranted?.Invoke();
        }

        private static void PermissionCallbacks_PermissionDenied(string permissionName)
        {
            Debug.Log($"{permissionName} Permission Denied");
        }
    }
}