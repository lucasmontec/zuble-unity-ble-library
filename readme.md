# ZuBle Library

## Zumbie Bluetooth Low Energy Unity Library

This is a simple wrapper around android's BLE capabilities. This is a complete jerry-rig implementation. I'll try to improve on-demand. This library currently only handles a single device per connection.

## To-Dos

* [x] Multi-Device with internal hashmap.
* [ ] Bonding.
* [ ] Notifications and indications.
* [x] MTU Size customization.
* [x] ValueTasks instead of listeners.

## Usage

See the ZuBleTestUIController on the unity example project.

### Initialization

This is a required step to find and create the java counter-part objects needed for our BLE calls.

``````csharp
private void Start()
{
    BleManager.Initialize();
}
``````

### Scanning devices

``````csharp
//Call this to start a scan
BleManager.StartScanning();
``````

Start scanning will start a BLE scan. This will trigger BLE scanning with some expensive configurations:

``````java
ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .setReportDelay(0)
                .build();
``````

The idea is to scan very briefly for your device, then connect to it using it's address. Scanning will run indefinitely so remember to call stop at some point:

``````csharp
//Call this to stop scanning
BleManager.StopScanning();
``````

#### Scan Results

You can have each scan result by subscribing to `BleManager.OnDeviceFound` or you can just pool the devices list directly: `BleManager.GetDevices()`.

#### Filtering by Services

To be implemented.

### Create bond

Not yet implemented.

### Connecting

To connect to a device you must have its address. To connect just call:

``````csharp
BleManager.ConnectToDevice(device.Address);

//Use these events to check if the connection went well
BleManager.OnConnected += ShowConnectedStatusText;
BleManager.OnDisconnected += ShowDisconnectedStatus;
``````

Service discovery is automatically called for a connected device. You can hook up to the callback when services are done discovering here:

``````csharp
BleManager.OnServicesDiscovered += ShowServicesDiscovered;
``````

[NOTE] All events will be converted into value tasks.

### Writing to a characteristic

Only works if connected to a device.

``````csharp
BleManager.WriteCharacteristic(serviceIdField.text, characteristicIdField.text, valueField.text);
``````