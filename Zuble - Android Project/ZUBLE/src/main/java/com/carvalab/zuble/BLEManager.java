package com.carvalab.zuble;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCallback;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.bluetooth.BluetoothManager;
import android.bluetooth.le.BluetoothLeScanner;
import android.bluetooth.le.ScanCallback;
import android.bluetooth.le.ScanFilter;
import android.bluetooth.le.ScanResult;
import android.bluetooth.le.ScanSettings;
import android.content.Context;
import android.util.Log;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

@SuppressLint("MissingPermission")
public class BLEManager {
    private final BluetoothAdapter bluetoothAdapter;
    private BluetoothLeScanner bluetoothLeScanner;

    private boolean isScanning = false;

    private BLEDeviceFoundCallback deviceFoundCallback = null;
    private BLEConnectionCallback connectionCallback = null;
    private BLEReadCallback readCallback = null;
    private BLEWriteCallback writeCallback = null;

    private BLERssiUpdateListener rssiUpdateListener = null;

    private final List<String> scannedDevices = new ArrayList<>();

    private final Context context;

    private final HashMap<String, BluetoothGatt> connectedDevices = new HashMap<>();
    private final HashMap<String, Boolean> servicesDiscovered = new HashMap<>();

    public String lastError = "no error";

    private final BLEWriteManager writeManager;

    public BLEManager(Context context) {
        final BluetoothManager bluetoothManager = (BluetoothManager) context.getSystemService(Context.BLUETOOTH_SERVICE);
        bluetoothAdapter = bluetoothManager.getAdapter();
        if (bluetoothAdapter != null) {
            bluetoothLeScanner = bluetoothAdapter.getBluetoothLeScanner();
        }

        this.context = context;
        this.writeManager = new BLEWriteManager(this, 50, 5);
    }

    private final BluetoothGattCallback gattCallback = new BluetoothGattCallback() {
        @Override
        public void onConnectionStateChange(BluetoothGatt gatt, int status, int newState) {
            super.onConnectionStateChange(gatt, status, newState);

            String address = gatt.getDevice().getAddress();

            if (newState == BluetoothGatt.STATE_CONNECTED) {
                if (connectionCallback != null) {
                    connectionCallback.onConnected(address);
                    discoverServices(address);
                }
            } else if (newState == BluetoothGatt.STATE_DISCONNECTED) {
                if (connectionCallback != null) {
                    connectionCallback.onDisconnected(address);
                    connectedDevices.remove(address);
                    servicesDiscovered.remove(address);
                }
            }
        }

        @Override
        public void onCharacteristicRead(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic,
                                         byte[] value, int status) {
            super.onCharacteristicRead(gatt, characteristic, value, status);

            String characteristicId = characteristic.getUuid().toString();
            String deviceAddress = gatt.getDevice().getAddress();

            Log.w("BLEManager", deviceAddress+" Characteristic read: "+characteristicId);

            if (status == BluetoothGatt.GATT_SUCCESS) {
                if (readCallback != null) {
                    readCallback.onRead(deviceAddress, characteristicId, value);
                }else{
                    Log.w("BLEManager", "No read callback!");
                }
            } else {
                Log.w("BLEManager", "Failed to read characteristic: status "+status);
                if (readCallback != null) {
                    readCallback.onReadFailed(deviceAddress, characteristicId, status);
                }
            }
        }

        @Override
        public void onReadRemoteRssi(BluetoothGatt gatt, int rssi, int status) {
            if (status == BluetoothGatt.GATT_SUCCESS) {
                if(rssiUpdateListener != null){
                    rssiUpdateListener.onRssiUpdated(rssi);
                }
                Log.d("BLEManager", "Remote RSSI: " + rssi);
            } else {
                Log.w("BLEManager", "Failed to read rssi: status "+status);
            }
        }

        @Override
        public void onServicesDiscovered(BluetoothGatt gatt, int status) {
            String address = gatt.getDevice().getAddress();

            if (status == BluetoothGatt.GATT_SUCCESS) {
                Log.d("BLEManager", "Services discovered");
                connectionCallback.onServicesDiscovered(address);
                servicesDiscovered.put(address, true);

            } else {
                Log.w("BLEManager", "Service discovery failed with status: " + status);
                connectionCallback.onServicesDiscoveryFailed(address, status);
                servicesDiscovered.put(address, false);
            }
        }

        @Override
        public void onMtuChanged(BluetoothGatt gatt, int mtu, int status) {
            super.onMtuChanged(gatt, mtu, status);
            String address = gatt.getDevice().getAddress();
            if (status == BluetoothGatt.GATT_SUCCESS) {
                connectionCallback.onMtuChangeSuccess(address, mtu);
                Log.i("BLEManager", "MTU changed successfully.");
            }else{
                connectionCallback.onMtuChangeFailed(address, status);
                Log.w("BLEManager", "MTU change failed with status: " + status);
            }
        }

        @Override
        public void onCharacteristicWrite(BluetoothGatt gatt, BluetoothGattCharacteristic characteristic, int status) {
            super.onCharacteristicWrite(gatt, characteristic, status);

            String address = gatt.getDevice().getAddress();
            String characteristicUUid = characteristic.getUuid().toString();

            if (status == BluetoothGatt.GATT_SUCCESS) {
                writeCallback.onCharacteristicWritten(address, characteristicUUid);
            }else{
                writeCallback.onCharacteristicWriteFailed(address, characteristicUUid, status);
            }
        }
    };

    public boolean areServicesDiscovered(String address) {
        return servicesDiscovered.containsKey(address) &&
                Boolean.TRUE.equals(servicesDiscovered.get(address));
    }

    public void connectToDevice(String address) {
        if (bluetoothAdapter == null || address == null) {
            Log.e("BLEManager", "BluetoothAdapter not initialized or unspecified address.");
            return;
        }

        servicesDiscovered.put(address, false);

        BluetoothDevice device = bluetoothAdapter.getRemoteDevice(address);
        if (device != null) {
            // Connect to the device. This method is asynchronous and the connection result
            // is delivered through the BluetoothGattCallback.
            BluetoothGatt bluetoothGatt = device.connectGatt(context, false, gattCallback);
            connectedDevices.put(address, bluetoothGatt);
        } else {
            Log.w("BLEManager", "Device not found "+address);
        }
    }

    public void disconnectDevice(String address) {
        if(!connectedDevices.containsKey(address)){
            return;
        }

        BluetoothGatt bluetoothGatt = connectedDevices.get(address);
        if (bluetoothGatt != null) {
            bluetoothGatt.disconnect();
            bluetoothGatt.close();
            connectedDevices.remove(address);
            servicesDiscovered.remove(address);
        }
    }

    public boolean isBluetoothEnabled() {
        return bluetoothAdapter != null && bluetoothAdapter.isEnabled();
    }

    public void setRssiListener(BLERssiUpdateListener listener) {
        this.rssiUpdateListener = listener;
    }

    public void setDeviceFoundCallback(BLEDeviceFoundCallback callback) {
        this.deviceFoundCallback = callback;
    }

    public void setConnectionCallback(BLEConnectionCallback callback) {
        this.connectionCallback = callback;
    }

    public void setReadCallback(BLEReadCallback callback) {
        this.readCallback = callback;
    }

    public void setWriteCallback(BLEWriteCallback callback) {
        this.writeCallback = callback;
    }

    public void requestMtu(String address, int mtu) {
        if(!connectedDevices.containsKey(address)){
            Log.w("BLEManager", "No connected device for address: "+address);
            return;
        }
        BluetoothGatt bluetoothGatt = connectedDevices.get(address);
        if (bluetoothGatt == null) {
            Log.e("BLEManager", "null device for address!");
            connectedDevices.remove(address);
            return;
        }
        bluetoothGatt.requestMtu(mtu);
    }

    public void readRemoteRssi(String address) {
        if(!connectedDevices.containsKey(address)){
            Log.w("BLEManager", "No connected device for address: "+address);
            return;
        }

        BluetoothGatt bluetoothGatt = connectedDevices.get(address);
        if (bluetoothGatt == null) {
            Log.e("BLEManager", "null device for address!");
            connectedDevices.remove(address);
            return;
        }
        bluetoothGatt.readRemoteRssi();
    }

    public boolean readCharacteristic(String address, String serviceUuidString, String characteristicUuidString) {
        if(!connectedDevices.containsKey(address)){
            lastError = "No connected device for address: "+address;
            Log.w("BLEManager", lastError);
            return false;
        }

        BluetoothGatt bluetoothGatt = connectedDevices.get(address);

        if (bluetoothGatt == null) {
            lastError = "Null device for address: "+address;
            Log.w("BLEManager", lastError);
            connectedDevices.remove(address);
            return false;
        }

        UUID serviceUuid = convertToUUID(serviceUuidString);
        UUID characteristicUuid = convertToUUID(characteristicUuidString);

        Log.d("BLEManager", "Starting characteristic read: " + serviceUuid + " " +characteristicUuid);

        BluetoothGattService service = bluetoothGatt.getService(serviceUuid);
        if (service == null) {
            lastError = "Service not found: " + serviceUuidString;
            Log.w("BLEManager", lastError);
            return false;
        }

        Log.d("BLEManager", "Service found");

        BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUuid);
        if (characteristic == null) {
            lastError = "Characteristic not found: " + characteristicUuidString;
            Log.w("BLEManager", lastError);
            return false;
        }

        Log.d("BLEManager", "Characteristic found");

        if ((characteristic.getProperties() & BluetoothGattCharacteristic.PROPERTY_READ) == 0) {
            lastError = "Failed to read characteristic: not readable!";
            Log.w("BLEManager", lastError);
            Log.d("BLEManager", "Characteristic properties: " + characteristic.getProperties());
            return false;
        }

        boolean readSuccess = false;
        try {
            readSuccess = bluetoothGatt.readCharacteristic(characteristic);
            Log.d("BLEManager", "Read status: "+readSuccess);
        }catch (Exception e) {
            lastError = "Failed to read characteristic: " + e.getMessage();
            Log.e("BLEManager", lastError);
            Log.d("BLEManager", "Characteristic properties: " + characteristic.getProperties());
        }

        if(!readSuccess) {
            lastError = "Failed to read characteristic: " + characteristicUuidString;
            Log.w("BLEManager", lastError);
        }
        return readSuccess;
    }

    public String getLastError() {
        Log.d("BLEManager", lastError);
        return lastError;
    }

    private UUID convertToUUID(String uuidString) {
        if (uuidString.length() == 4) {
            return UUID.fromString("0000" + uuidString.toUpperCase() + "-0000-1000-8000-00805F9B34FB");
        }
        return UUID.fromString(uuidString.toUpperCase());
    }

    public void writeCharacteristic(String address, String serviceUuidString, String characteristicUuidString, byte[] data) {
        writeManager.writeCharacteristic(address, serviceUuidString, characteristicUuidString, data);
    }

    public void writeCharacteristic(String address, String serviceUuidString, String characteristicUuidString, String base64EncodedData, boolean dontDecode) {
        writeManager.writeCharacteristic(address, serviceUuidString, characteristicUuidString, base64EncodedData, dontDecode);
    }

    public void discoverServices(String address) {
        Log.w("BLEManager", "No connected device for address: "+address);
        if(!connectedDevices.containsKey(address)){
            return;
        }

        BluetoothGatt bluetoothGatt = connectedDevices.get(address);
        if (bluetoothGatt == null) {
            Log.e("BLEManager", "Null device for: "+address);
            connectedDevices.remove(address);
            servicesDiscovered.remove(address);
            return;
        }

        boolean startedDiscovery = bluetoothGatt.discoverServices();
        Log.d("BLEManager", "Service discovery started: " + startedDiscovery);
    }

    private final ScanCallback scanCallback = new ScanCallback() {
        @Override
        public void onScanResult(int callbackType, ScanResult result) {
            BluetoothDevice device = result.getDevice();

            String deviceName = device.getName();
            String deviceAddress = device.getAddress();

            Log.d("BLEManager", "Device found: " + deviceName + " - " + deviceAddress);

            String deviceInfo = deviceName + "|" + deviceAddress;
            if (!scannedDevices.contains(deviceInfo)) {
                scannedDevices.add(deviceInfo);
            }

            if (deviceFoundCallback != null) {
                deviceFoundCallback.onDeviceFound(deviceName, deviceAddress);
            }

            super.onScanResult(callbackType, result);
        }

        @Override
        public void onBatchScanResults(List<ScanResult> results) {
            super.onBatchScanResults(results);
            try {
                for (ScanResult result : results) {
                    Log.i("ScanResult - Results", result.toString() + " " + result.getDevice().getName());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        @Override
        public void onScanFailed(int errorCode) {
            super.onScanFailed(errorCode);
            Log.e("BLEManager", "Scan failed with error: " + errorCode);
        }
    };

    public void startScanning() {
        if (isScanning) {
            return;
        }

        if (bluetoothLeScanner == null) {
            Log.w("BLEManager", "No bluetoothLeScanner available!");
            return;
        }

        scannedDevices.clear();
        isScanning = true;

        ArrayList<ScanFilter> filters = new ArrayList<>();

        ScanSettings settings = new ScanSettings.Builder()
                .setScanMode(ScanSettings.SCAN_MODE_LOW_LATENCY)
                .setCallbackType(ScanSettings.CALLBACK_TYPE_ALL_MATCHES)
                .setReportDelay(0)
                .build();

        bluetoothLeScanner.startScan(filters, settings, scanCallback);
        Log.i("BLEManager", "Scanning started...");
    }

    public void stopScanning() {
        if (!isScanning) {
            return;
        }
        isScanning = false;

        if (bluetoothLeScanner == null) {
            Log.w("BLEManager", "No scanner set.");
            return;
        }

        bluetoothLeScanner.stopScan(scanCallback);
        Log.i("BLEManager", "Scanning stopped.");
    }

    public String getScannedDevices() {
        return String.join(",", scannedDevices);
    }

    public String getConnectedDevices() {
        return String.join(",", connectedDevices.keySet());
    }

    public BluetoothGatt getConnectedDevice(String address) {
        return connectedDevices.get(address);
    }

    public void removeConnectedDevice(String address) {
        connectedDevices.remove(address);
    }
}
