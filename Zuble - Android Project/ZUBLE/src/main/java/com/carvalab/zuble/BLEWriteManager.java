package com.carvalab.zuble;

import android.annotation.SuppressLint;
import android.bluetooth.BluetoothGatt;
import android.bluetooth.BluetoothGattCharacteristic;
import android.bluetooth.BluetoothGattService;
import android.os.Handler;
import android.os.Looper;
import android.util.Base64;
import android.util.Log;
import android.bluetooth.BluetoothStatusCodes;

import java.nio.charset.StandardCharsets;
import java.util.ArrayDeque;
import java.util.Queue;
import java.util.UUID;

@SuppressLint("MissingPermission")
public class BLEWriteManager {
    private final BLEManager bleManager;

    private final Queue<BLEWriteAction> writeQueue = new ArrayDeque<>();
    private final Handler handler = new Handler(Looper.getMainLooper());

    private final int retryTimeMillis;

    private static final int maxQueueSize = 10000;

    private int currentRetryCount;
    private int maxRetriesPerAction = 5;
    private int writeType = BluetoothGattCharacteristic.WRITE_TYPE_NO_RESPONSE;

    public BLEWriteManager(BLEManager bleManager, int retryTimeMillis, int maxRetriesPerAction, int writeType) {
        this.bleManager = bleManager;
        this.retryTimeMillis = retryTimeMillis;
        this.maxRetriesPerAction = maxRetriesPerAction;
        this.writeType = writeType;
    }

    public BLEWriteManager(BLEManager bleManager, int retryTimeMillis, int maxRetriesPerAction) {
        this.bleManager = bleManager;
        this.retryTimeMillis = retryTimeMillis;
        this.maxRetriesPerAction = maxRetriesPerAction;
    }

    public boolean writeCharacteristic(String address, String serviceUuidString, String characteristicUuidString, byte[] data) {
        BLEWriteAction action = new BLEWriteAction(address, serviceUuidString, characteristicUuidString, data);
        return attemptWrite(action);
    }

    public boolean writeCharacteristic(String address, String serviceUuidString, String characteristicUuidString, String base64EncodedData, boolean dontDecode) {
        byte[] data = dontDecode ?
                base64EncodedData.getBytes(StandardCharsets.UTF_8) :
                Base64.decode(base64EncodedData, Base64.DEFAULT);
        BLEWriteAction action = new BLEWriteAction(address, serviceUuidString, characteristicUuidString, data);
        return attemptWrite(action);
    }

    private boolean attemptWrite(BLEWriteAction action) {
        boolean didWrite = writeCharacteristicInternal(action);

        if (didWrite) {
            currentRetryCount = 0;
            return true;
        }

        if(currentRetryCount > maxRetriesPerAction) {
            Log.w("BLEWriteManager", "Exceeded max retries for action: " + action.address);
            writeQueue.poll();
            currentRetryCount = 0;
            return false;
        }

        currentRetryCount++;

        synchronized (writeQueue) {
            if(writeQueue.size() > maxQueueSize) {
                Log.w("BLEWriteManager", "attemptWrite: Write queue is full! Ignored write");
                return false;
            }
            writeQueue.add(action);
            scheduleNextWrite();
        }

        return false;
    }

    private void scheduleNextWrite() {
        handler.postDelayed(this::processNextWrite, retryTimeMillis);
    }

    private void processNextWrite() {
        synchronized (writeQueue) {
            if (writeQueue.isEmpty()) {
                return;
            }
            BLEWriteAction action = writeQueue.poll();
            attemptWrite(action);
        }
    }

    private boolean writeCharacteristicInternal(BLEWriteAction action) {
        BluetoothGatt bluetoothGatt = bleManager.getConnectedDevice(action.address);
        if (bluetoothGatt == null) {
            Log.e("BLEWriteManager", "Null device for: " + action.address);
            bleManager.removeConnectedDevice(action.address);
            return false;
        }

        UUID serviceUuid = UUID.fromString(action.serviceUuidString);
        UUID characteristicUuid = UUID.fromString(action.characteristicUuidString);
        BluetoothGattService service = bluetoothGatt.getService(serviceUuid);
        if (service == null) {
            Log.w("BLEWriteManager", "Service not found: " + action.serviceUuidString);
            return false;
        }

        BluetoothGattCharacteristic characteristic = service.getCharacteristic(characteristicUuid);
        if (characteristic == null) {
            Log.w("BLEWriteManager", "Characteristic not found: " + action.characteristicUuidString);
            return false;
        }

        int status = bluetoothGatt.writeCharacteristic(characteristic, action.data, writeType);

        return handleInternalWriteStatus(status);
    }

    private static boolean handleInternalWriteStatus(int status) {
        switch (status) {
            case BluetoothStatusCodes.SUCCESS:
                return true;
            case BluetoothStatusCodes.ERROR_GATT_WRITE_NOT_ALLOWED:
                Log.w("BLEWriteManager", "Write failed: Write not allowed.");
                break;
            case BluetoothStatusCodes.ERROR_GATT_WRITE_REQUEST_BUSY:
                Log.w("BLEWriteManager", "Write failed: Device or request busy.");
                break;
            case BluetoothStatusCodes.ERROR_DEVICE_NOT_BONDED:
                Log.w("BLEWriteManager", "Write failed: Device not bonded.");
                break;
            case BluetoothStatusCodes.ERROR_PROFILE_SERVICE_NOT_BOUND:
                Log.w("BLEWriteManager", "Write failed: Profile service not bound.");
                break;
            case BluetoothStatusCodes.ERROR_MISSING_BLUETOOTH_CONNECT_PERMISSION:
                Log.w("BLEWriteManager", "Write failed: Missing Bluetooth connect permission.");
                break;
            case BluetoothStatusCodes.ERROR_UNKNOWN:
            default:
                Log.w("BLEWriteManager", "Write failed with unknown or unhandled error code: " + status);
                break;
        }
        return false;
    }

    private static class BLEWriteAction {
        final String address;
        final String serviceUuidString;
        final String characteristicUuidString;
        final byte[] data;

        BLEWriteAction(String address, String serviceUuidString, String characteristicUuidString, byte[] data) {
            this.address = address;
            this.serviceUuidString = serviceUuidString;
            this.characteristicUuidString = characteristicUuidString;
            this.data = data;
        }
    }
}
