package com.carvalab.zuble;

public interface BLERssiUpdateListener {
    void onRssiUpdated(int rssi);
}