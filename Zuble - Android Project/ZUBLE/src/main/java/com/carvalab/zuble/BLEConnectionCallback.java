package com.carvalab.zuble;

public interface BLEConnectionCallback {
    void onConnected(String deviceAddress);
    void onServicesDiscovered(String deviceAddress);
    void onServicesDiscoveryFailed(String deviceAddress, int status);
    void onDisconnected(String deviceAddress);
    void onMtuChangeSuccess(String deviceAddress, int mtu);
    void onMtuChangeFailed(String deviceAddress, int status);

    void onCharacteristicWritten(String address, String characteristicUUid);
    void onCharacteristicWriteFailed(String address, String characteristicUUid, int status);
}