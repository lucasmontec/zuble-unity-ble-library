package com.carvalab.zuble;

public interface BLEDeviceFoundCallback {
    void onDeviceFound(String deviceName, String deviceAddress);
}