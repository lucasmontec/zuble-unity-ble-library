package com.carvalab.zuble;

public interface BLEWriteCallback {
    void onCharacteristicWritten(String address, String characteristicUUid);
    void onCharacteristicWriteFailed(String address, String characteristicUUid, int status);
}
