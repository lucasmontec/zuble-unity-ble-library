package com.carvalab.zuble;

public interface BLEReadCallback {
    void onRead(String deviceAddress, String characteristicUuid, byte[] data);
    void onReadFailed(String address, String characteristicUuid, int status);
}